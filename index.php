<?php

class fromcoz {

    private $urls = array(
        '/load' => '/loads/',
        '/publ' => '/stat/',
        '/blog' => '/news/category/2',
        '/faq' => '/news/category/3',
        '/tests' => '/',
        '/dir' => '/',
        '/photo' => '/foto',
    );

	public function __construct($params = array()) 
	{
	}
	
	
	public function common($params = array()) 
	{
		$url = trim($_SERVER['REQUEST_URI']);
        if (substr($url, -1) == "/") {
            $url = substr($url, 0, strlen($url)-1);
        }
        if (array_key_exists($url, $this->urls)) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $this->urls[$url]);
            die();
        } else {
            $value = null;
            if (strpos($url, "?")) {
                $record = explode("/", substr($url, 0, strpos($url, "?")));
            } else {
                $record = explode("/", $url);
            }
            if (count($record) > 1) {
                $url_id = (count($record) > 2) ? $record[count($record) - 1] : null;
                if ($record[1] == "forum") { // Модуль "Форум"
                    $url_id = (count($record) > 2) ? $record[2] : null;
                    if ($url_id != null) {
                        $index = explode("-", $url_id);
                        if (count($index) > 0 && ($index[0] == "0")) {
                            if (count($index) >= 4) {
                                $page = !empty($index[2]) && $index[2] != "0" && $index[2] != "1";
                                if ($index[3] == "34") { // http://site.ucoz.ru/forum/0-0-1-34 Ленточный форум
                                    $value = "/forum/last_posts/" . ($page ? "?page=" . $index[2] : "");
                                    if (count($index) >= 5 && !empty($index[4])) {
                                        $value .= ($page ? "&" : "?") . "order=";
                                        if ($index[4] == "1") $value .= "title&asc=1";
                                        elseif ($index[4] == "2") $value .= "title";
                                        elseif ($index[4] == "3") $value .= "posts&asc=1";
                                        elseif ($index[4] == "4") $value .= "posts";
                                        elseif ($index[4] == "5") $value .= "views&asc=1";
                                        elseif ($index[4] == "6") $value .= "views";
                                        elseif ($index[4] == "7") $value .= ""; // TODO: Автор темы
                                        elseif ($index[4] == "8") $value .= ""; // TODO: Автор темы
                                        elseif ($index[4] == "9") $value .= "last_post&asc=1";
                                        elseif ($index[4] == "10") $value .= "last_post";
                                    }
                                } elseif ($index[3] == "35") { // http://site.ucoz.ru/forum/0-0-1-35 Пользователи форума
                                    $value = "/users/index/";
                                } elseif ($index[3] == "36") { // http://site.ucoz.ru/forum/0-0-0-36 Правила форума
                                    $value = "/forum/"; // TODO: Правила форума 
                                } elseif ($index[3] == "37") { // http://site.ucoz.ru/forum/0-0-0-37 RSS для форума
                                    $value = "/forum/rss/";
                                } elseif ($index[3] == "6") { // http://site.ucoz.ru/forum/0-0-0-6 Поиск для форума
                                    $value = "/search/";
                                } elseif ($index[3] = "3" && count($index) >= 5 && !empty($index[4])) { // http://site.ucoz.ru/forum/0-0-1-3-1 Сообщения пользователя
                                    $value = "/forum/user_posts/" . $index[4] . ($page ? "?page=" . $index[2] : "");
                                }
                            }
                        } elseif (count($index) == 1 || (count($index) > 1 && empty($index[1]))) {
                            // TODO: Здесь должен быть анализ на что идет ссылка - на форум или раздел
                            if (is_numeric($index[0])) {
                                $value = "/forum/view_forum/" . $index[0];
                            }
                        } elseif ((count($index) == 2 || (count($index) > 2 && empty($index[2]))) // http://site.ucoz.ru/forum/1-1 Тема форума
                                  || (count($index) == 3 || (count($index) > 3 && empty($index[3])))) { // http://site.ucoz.ru/forum/1-1-1 Тема форума с номером страницы
                            if ($index[1] == "0") {
                                $value = "/forum/view_forum/" . $index[0];
                            } else {
                                $value = "/forum/view_theme/" . $index[1];
                                $value = "/forum/view_theme/" . $index[1] . (count($index) > 2 && !empty($index[2]) && $index[2] != "0" && $index[2] != "1" ? "?page=" . $index[2] : "");
                            }
                        } elseif (count($index) >= 4) {
                            if (empty($index[2]) || $index[2] == "0") {
                                if (!empty($index[1]) && $index[1] == "17") { // http://site.ucoz.ru/forum/1-1-0-17-1 Последнее сообщение темы
                                    $value = "/forum/view_theme/" . $index[1]; // TODO: Последнее сообщение темы
                                } elseif ($index[1] == "0") {
                                    $value = "/forum/view_forum/" . $index[0];
                                } else {
                                    $value = "/forum/view_theme/" . $index[1];
                                }
                            } elseif ($index[3] == "16") { // http://site.ucoz.ru/forum/1-1-1-16-1350000000 Обработка ссылок на посты форума
                                $value = "/forum/view_post/" . $index[2];
                            }
                        }
                    }
                } elseif ($record[1] == "load") { // Модуль "Файловый архив"
                    $value = "/loads/"; // Обязательное перенаправление
                    if ($url_id != null) {
                        $index = explode("-", $url_id);
                        $page = count($index) > 1 && !empty($index[1]) && $index[1] != "0" && $index[1] != "1";
                        if (count($index) > 0 && count($index) < 4) { // http://site.ucoz.ru/load/1-1 Категория
                            if ($index[0] == "0") {
                                $value = "/loads/" . ($page ? "?page=" + $index[1] : "");
                            } elseif (is_numeric($index[0])) {
                                $value = "/loads/category/" . $index[0] . ($page ? "?page=" + $index[1] : "");
                            }
                        } elseif (count($index) >= 4 && $index[3] != null && !empty($index[3]) && $index[3] != "0") { // http://site.ucoz.ru/load/1-1-0-1 Материал
                            $value = "/loads/view/" . $index[3];
                        } elseif (count($index) == 5) {
                            if ($index[4] != null && !empty($index[4])) {
                                if ($index[4] == "1") { // http://site.ucoz.ru/load/0-0-0-0-1 Добавление материала
                                    $value = "/loads/add_form/";
                                } elseif ($index[4] == "16") { // Неактивные материалы и ТОП-ы
                                    $value = "/loads/" . ($page ? "?page=" . $index[1] : "");
                                    if (!empty($index[2])) {
                                        $value .= ($page ? "&" : "?") . "order=";
                                        if ($index[2] == "0") $value .= "date"; // TODO: http://site.ucoz.ru/load/0-1-0-0-16 Неактивные материалы
                                        elseif ($index[2] == "1") $value .= "date"; // http://site.ucoz.ru/load/0-1-1-0-16 Последние поступления (ТОП материалов, отсортированных по дате добавления)
                                        elseif ($index[2] == "2") $value .= "date"; // TODO: http://site.ucoz.ru/load/0-1-2-0-16 Лучшие материалы (ТОП материалов, отсортированных по рейтингу)
                                        elseif ($index[2] == "3") $value .= "downloads"; // http://site.ucoz.ru/load/0-1-3-0-16 Самые скачиваемые материалы (ТОП материалов, отсортированных по загрузкам)
                                        elseif ($index[2] == "4") $value .= "views"; // http://site.ucoz.ru/load/0-1-4-0-16 Самые читаемые материалы (ТОП материалов, отсортированных по просмотрам)
                                        elseif ($index[2] == "5") $value .= "comments"; // http://site.ucoz.ru/load/0-1-5-0-16 Самые комментируемые материалы (ТОП материалов, отсортированных по комментариям)
                                    }
                                } elseif ($index[4] == "17") { // http://site.ucoz.ru/load/0-0-1-0-17 Материалы пользователя
                                    $value = "/loads/" . (!empty($index[2]) && $index[2] != "0" ? "user/" . $index[2] : "") . ($page ? "?page=" . $index[1] : "");
                                } elseif ($index[4] == "13") { // http://site.ucoz.ru/load/0-0-0-1-13 Редактирование материала
                                    $value = "/loads/" . (!empty($index[3]) && $index[3] != "0" ? "edit_form/" . $index[3] : "");
                                } elseif ($index[4] == "20") { // http://site.ucoz.ru/load/0-0-0-1-20 Ссылка для скачивания материала
                                    $value = "/loads/" . (!empty($index[3]) && $index[3] != "0" ? "download_file/" . $index[3] : "");
                                }
                            }
                        }
                    }
                } elseif ($record[1] == "publ") { // Модуль "Статьи"
                    $value = "/stat/"; // Обязательное перенаправление
                    if ($url_id != null) {
                        $index = explode("-", $url_id);
                        $page = count($index) > 1 && !empty($index[1]) && $index[1] != "0" && $index[1] != "1";
                        if (count($index) > 0 && count($index) < 4) { // http://site.ucoz.ru/publ/1-1 Категория
                            if ($index[0] == "0") {
                                $value = "/stat/";
                            } elseif (is_numeric($index[0])) {
                                $value = "/stat/category/" . $index[0];
                            }
                            if ($value != null && count($index) > 1) {
                                $value .= "?page=" . $index[1];
                            }
                        } elseif (count($index) >= 4 && $index[3] != null && !empty($index[3]) && $index[3] != "0") { // http://site.ucoz.ru/publ/1-1-0-1 Материал
                            $value = "/stat/view/" . $index[3];
                        } elseif (count($index) == 5) {
                            if ($index[4] != null && !empty($index[4])) {
                                if ($index[4] == "1") { // http://site.ucoz.ru/publ/0-0-0-0-1 Добавление материала
                                    $value = "/stat/add_form/";
                                } elseif ($index[4] == "16") { // Неактивные материалы и ТОП-ы
                                    $value = "/stat/" . ($page ? "?page=" . $index[1] : "");
                                    if (!index[2].isEmpty()) {
                                        $value .= ($page ? "&" : "?") . "order=";
                                        if ($index[2] == "0") $value .= "date"; // TODO: http://site.ucoz.ru/publ/0-1-0-0-16 Неактивные материалы
                                        elseif ($index[2] == "1") $value .= "date"; // http://site.ucoz.ru/publ/0-1-1-0-16 Последние поступления (ТОП материалов, отсортированных по дате добавления)
                                        elseif ($index[2] == "2") $value .= "date"; // TODO: http://site.ucoz.ru/publ/0-1-2-0-16 Лучшие материалы (ТОП материалов, отсортированных по рейтингу)
                                        elseif ($index[2] == "3") $value .= "downloads"; // http://site.ucoz.ru/publ/0-1-3-0-16 Самые скачиваемые материалы (ТОП материалов, отсортированных по загрузкам)
                                        elseif ($index[2] == "4") $value .= "views"; // http://site.ucoz.ru/publ/0-1-4-0-16 Самые читаемые материалы (ТОП материалов, отсортированных по просмотрам)
                                        elseif ($index[2] == "5") $value .= "comments"; // http://site.ucoz.ru/publ/0-1-5-0-16 Самые комментируемые материалы (ТОП материалов, отсортированных по комментариям)
                                    }
                                } elseif ($index[4] == "17") { // http://site.ucoz.ru/publ/0-0-1-0-17 Материалы пользователя
                                    $value = "/stat/" . (!empty($index[2]) && $index[2] != "0" ? "user/" . $index[2] : "") . ($page ? "?page=" . $index[1] : "");
                                } elseif ($index[4].equals("13")) { // http://site.ucoz.ru/publ/0-0-0-1-13 Редактирование материала
                                    $value = "/stat/" . (!empty($index[3]) && $index[3] != "0" ? "edit_form/" . $index[3] : "");
                                }
                            }
                        }
                    }
                } elseif ($record[1] == "news" || $record[1] == "blog") { // Модуль "Новости" (ссылки на новости и блоги)
                    if ($record[1] == "blog") {
                        $value = "/news/category/2/"; // Обязательное перенаправление
                    }
//                    $ignore_actions = array("add", "add_form", "category", "delete", "download_file", "edit_form", "fix_on_top", "index", "off_home", "on_home", "premoder", "rss", "update", "upper", "user", "view");
//                    if (!in_array(strtolower($record[2]), $ignore_actions)) {
                    if (count($record) > 2 && $url_id != null && (is_numeric($url_id) || strpos('-', $url_id) !== false)) {
                        $index = explode("-", $url_id);
                        if (count($index) > 0 && count($index) < 4) {
                            if (count($index) == 1) { // http://site.ucoz.ru/news/2 2-я страница архива новостей
                                $page = !empty($index[0]) && $index[0] != "0" && $index[0] != "1";
                                $value = "/news/" . ($page ? "?page=" . $index[0] : "");
                            } elseif (count($index) == 2 && !empty($index[1])) {
                                if ($index[1] == "00") { // TODO: http://site.ucoz.ru/news/2011-00 Календарь с сообщениями за 2011 год
                                } else { // TODO: http://site.ucoz.ru/news/2011-02 Календарь с сообщениями за февраль 2011 года
                                }
                            } elseif (count($index) == 3 && !empty($index[1]) && !empty($index[2])) {
                                if ($index[1] == "0") { // http://site.ucoz.ru/news/2-0-1 2-я страница категории
                                    $page = !empty($index[0]) && $index[0] != "0" && $index[0] != "1";
                                    $id = (intval($index[2]) + 1) * 3 + ($record[1] == "news" ? 1 : 2);
                                    $value = "/news/category/" . $id . ($page ? "?page=" . $index[0] : "");
                                } else { // TODO: http://site.ucoz.ru/news/2011-02-06 Сообщения за 6 февраля 2011 года
                                }
                            }
                        } elseif (count($index) >= 4 && $index[3] != null && !empty($index[3]) && $index[3] != "0") { // http://site.ucoz.ru/news/2011-02-06-1 Материал
                            $id = (intval($index[3]) - 1) * 3 + ($record[1] == "news" ? 1 : 2);
                            $value = "/news/view/" . $id;
                        } elseif (count($index) == 5) {
                            if ($index[4] != null && !empty($index[4])) {
                                if ($index[4] == "1") { // Добавление материала
                                    $value = "/news/add_form/";
                                }
                            }
                        }
                    }
                } elseif ($record[1] == "faq") { // Модуль "Новости" (ссылки на FAQ)
                    $value = "/news/category/3/"; // Обязательное перенаправление
                    if ($url_id != null) {
                        $index = explode("-", $url_id);
                        if (count($index) > 0 && count($index) < 3) {
                            $page = count($index) > 1 && !empty($index[1]) && $index[1] != "0" && $index[1] != "1";
                            if (empty($index) || $index[0] == "0") { // http://site.ucoz.ru/faq/0-2 2-я страница FAQ
                                $value = "/news/" . ($page ? "?page=" . $index[1] : "");
                            } elseif (is_numeric($index[0])) { // http://site.ucoz.ru/faq/1-2 2-я страница категории
                                $id = (intval($index[0]) + 1) * 3 + 3;
                                $value = "/news/category/" . $id . ($page ? "?page=" . $index[1] : "");
                            }
                        } elseif (count($index) >= 3 && $index[2] != null && !empty($index[2]) && $index[2] != "0") { // http://site.ucoz.ru/faq/1-0-9 Материал
                            $id = (intval($index[3]) - 1) * 3 + 3;
                            $value = "/news/view/" . $id;
                        }
                    }
                } elseif ($record[1] == "photo") {
                    $value = "/foto/";
                } elseif ($record[1] == "index") {
                    if ($url_id != null) {
                        $index = explode("-", $url_id);
                        if ($index[0] == "15") {
                            // Пользователи сайта
                            $value = "/users/index/";
                        } elseif ($index[0] == "1") {
                            // Страница входа
                            $value = "/users/login_form/";
                        } elseif ($index[0] == "3") {
                            // Страница регистрации
                            $value = "/users/add_form/";
                        } elseif ($index[0] == "34") {
                            // Комментарии пользователя index[1]
                        } elseif ($index[0] == "8") {
                            // Профиль пользователя с номером index[1] или именем index[2]
                            if (count($index) == 2) {
                                $value = "/users/info/" . $index[1];
                            } elseif (count($index) == 3 && $index[1] == "0") {
                                $value = "/users/index/";
                            }
                        }
                    }
                }
            }

            if ($value != null) {
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: ' . $value);
                die();
            }
        }
	}
}
